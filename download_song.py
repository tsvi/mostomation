#!/usr/bin/env python3

import sys

import youtube_dl

print(f"Downloading {sys.argv[1]}")

ydl_opts = {
    'continuedl': False,
    'format': '140',
    'max_downloads': 1,
    'outtmpl': '/config/www/naama_alarm_clock.m4a',
}

with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    ydl.download([sys.argv[1]])

External scripts
################

This directory holds scripts that are used to setup HASS.

**switcher.py**
   - Script to extract the login info for the Switcher
   - Runs on Python 2.7 (``$ python switcher.py``)
   - Select "extract" option and follow the instructions
